package com.mm.ignis

import kotlin.random.Random


private val digits = "0123456789"
private val letters = "qwertyuiopasdfghjklzxcvbnm"

fun randomKey(length: Int) = buildString {
    repeat(length) {
        val str = if (Random.nextBoolean()) digits else letters
        append(str[Random.nextInt(str.length)])
    }
}
