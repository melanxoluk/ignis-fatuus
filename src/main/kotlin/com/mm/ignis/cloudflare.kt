package com.mm.ignis

import eu.roboflax.cloudflare.CloudflareAccess
import eu.roboflax.cloudflare.CloudflareRequest
import eu.roboflax.cloudflare.constants.Category
import eu.roboflax.cloudflare.objects.dns.DNSRecord
import eu.roboflax.cloudflare.objects.zone.Zone
import java.time.Instant


data class CloudflareService(
    val id: Long = -1,
    val name: String,
    val apiKey: String,
    val email: String,
    val createdOn: Instant
)

data class CloudflareSite(
    val id: Long = -1,
    val cloudflareServiceId: Long,
    val originId: String,
    val name: String,
    val createdOn: Instant,
    val savedOn: Instant
) {
    var cloudflareService: CloudflareService? = null
}

// setting redirection rules to available sites
// US: Create project
// - setting project name, url-name, gitlab project, cloudflare site
// - create project base on existing site 'melancholy-millennium.com'
// - for every gitlab project branch create non creating ignis project with url name 'example' there are new urls
// todo support first-level domains <appname>.com
class Cloudflare(private val db: Database) {
    fun getServices() = db.selectCloudflareServices()

    fun findSite(name: String): Pair<CloudflareService, Zone>? {
        for (service in getServices()) {
            val access = service.access()

            val zones =
                CloudflareRequest(Category.LIST_ZONES, access)
                    .asObjectList(Zone::class.java)
                    .`object`

            for (zone in zones) {
                if (zone.name == name) {
                    return service to zone
                }
            }
        }

        return null
    }

    fun createRecord(
        service: CloudflareService,
        zone: Zone,
        target: String,
        origin: String
    ): DNSRecord {
        val access = service.access()

        val resp =
            CloudflareRequest(Category.CREATE_DNS_RECORD, access)
                .identifiers(zone.id)
                .body("type", "CNAME")
                .body("name", target)
                .body("content", origin)
                .body("proxied", true)
                .asObject(DNSRecord::class.java)

        return resp.`object`
    }

    fun deleteRecord(
        project: IgnisProject,
        site: IgnisProjectSite
    ) {
        val access = project.cloudflareSite.cloudflareService!!.access()

        CloudflareRequest(Category.DELETE_DNS_RECORD, access)
            .identifiers(project.cloudflareSite.originId)
            .identifiers(site.dnsRecordOriginId)
            .asVoid()
    }

    private fun CloudflareService.access() = CloudflareAccess(apiKey, email)
}
