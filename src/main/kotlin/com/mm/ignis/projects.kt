package com.mm.ignis

import eu.roboflax.cloudflare.objects.zone.Zone
import org.gitlab4j.api.models.Project


data class Project(
    val id: Long,
    // any symbols
    val name: String,
    // part of url
    val urlName: String,
    val gitlabProject: Project,
    val cloudflareSite: Zone,
    val key: String)

class Projects {
    fun getProjectByKey(key: String) {

    }
}