package com.mm.ignis

import org.gitlab4j.api.Constants
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.*
import org.gitlab4j.api.models.Project
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.time.Instant


data class GitlabService(
    val id: Long = -1,
    val name: String,
    val url: String,
    val accessKey: String,
    val createdOn: Instant = Instant.now()
) {
    var remoteProjects: List<Project>? = null
    var projects: List<GitlabProject>? = null
}

data class GitlabProject(
    val id: Long = -1,
    val gitlabServiceId: Long,
    val originId: Int,
    val name: String,
    val namespaceName: String,
    val path: String,
    val namespacePath: String,
    val webUrl: String,
    val createdOn: Instant,
    val savedOn: Instant
) {
    var gitlabService: GitlabService? = null
}

data class GitlabProjectHook(
    val id: Long = -1,
    val gitlabProject: Long,
    val originId: Int,
    val key: String,
    val url: String
)


class Gitlab(private val db: Database) {
    private companion object {
        private val log = LoggerFactory.getLogger(Gitlab::class.java)
    }


    // ~~~ services

    fun savedServices() = db.selectGitlabServices()

    fun createService(service: GitlabService) {
        db.saveGitlabService(service)
    }


    // ~~~ projects

    fun findProject(name: String): Pair<GitlabService, Project>? {
        for (service in savedServices()) {
            try {
                val api = GitLabApi(service.url, service.accessKey)
                val ownedProjects = api.projectApi.ownedProjects
                for (project in ownedProjects) {
                    if (project.name == name) {
                        return service to project
                    }
                }
            } catch (ex: Exception) {
                log.error("gitlab api exception for service $service", ex)
            }
        }

        return null
    }

    fun getProject(service: GitlabService, name: String): Project {
        val api = GitLabApi(service.url, service.accessKey)
        return api.projectApi.getProject(name)
    }

    fun getProject(gitlabProject: GitlabProject): Project {
        if (gitlabProject.gitlabService == null) {
            gitlabProject.gitlabService = db.selectGitlabService(gitlabProject)
        }

        val api = gitlabProject.gitlabService!!.api()
        return api.projectApi.getProject(gitlabProject.originId.toInt())
    }

    fun createWebhook(service: GitlabService, project: Project, url: String): ProjectHook {
        val api = GitLabApi(service.url, service.accessKey)

        val hook = ProjectHook().apply {
            pushEvents = true
            pipelineEvents = true
        }

        return api.projectApi.addHook(project, url, hook, false, null)
    }

    fun getBranches(service: GitlabService, project: Project): List<Branch> {
        val api = GitLabApi(service.url, service.accessKey)
        return api.repositoryApi.getBranches(project.id)
    }

    fun lastArtifact(service: GitlabService, project: Project, branch: String): ByteArray? {
        val api = GitLabApi(service.url, service.accessKey)

        val pipelines =
            api.pipelineApi.getPipelines(
                project.id,
                Constants.PipelineScope.BRANCHES,
                PipelineStatus.SUCCESS,
                branch,
                false,
                null,
                null,
                null,
                null,
                1,
                1
            )

        if (pipelines.isEmpty()) {
            log.info("not found success pipelines in branch $branch on project $project")
            return null
        }

        val lastSuccessPipeline = pipelines[0]

        return pipelineArtifact(service, project, lastSuccessPipeline.id)
    }

    fun pipelineArtifact(service: GitlabService, project: Project, pipelineId: Int): ByteArray? {
        val api = service.api()
        val jobs = api.jobApi.getJobsForPipeline(project, pipelineId)
        val artifactSuccessJobs = jobs.firstOrNull {
            it.status == JobStatus.SUCCESS && it.artifactsFile != null
        }
        if (artifactSuccessJobs == null) {
            log.info("not found success job with artifact in pipeline $pipelineId on project $project")
            return null
        }

        return api.jobApi
            .downloadArtifactsFile(project, artifactSuccessJobs.id)
            .readBytes()
    }

    fun removeWebHook(service: GitlabService, ignisProject: IgnisProject) {
        val api = service.api()
        api.projectApi.deleteHook(ignisProject.gitlabProject.id.toInt(), ignisProject.gitlabHookId.toInt())
    }

    private fun GitlabService.api() = GitLabApi(url, accessKey)
}

fun main(args: Array<String>) {
    val key = "z8sZzYQ5cYXwhTPU7inj"
    val api = GitLabApi("https://gitlab.leandi.net", key)
    val project = api.projectApi.ownedProjects.first { it.name == "web-client" }

    val pipelines =
        api.pipelineApi.getPipelines(
            project.id,
            Constants.PipelineScope.BRANCHES,
            PipelineStatus.SUCCESS,
            "master",
            false,
            null,
            null,
            null,
            null,
            1,
            1
        )

    val lastSuccessPipeline = pipelines[0]
    val job1 = api.jobApi.getJobsForPipeline(project, lastSuccessPipeline.id)
    val job = api.jobApi.getJob(project.id, 47)
    val bytes = api.jobApi.downloadArtifactsFile(project.id, 47).readBytes()
    Files.write(Paths.get("zip.zip"), bytes, StandardOpenOption.CREATE_NEW)

    // val jobs = api.jobApi.getJobs(project.id)
    // val successJobs = api.jobApi.getJobs(project.id, Constants.JobScope.SUCCESS)
    println(job)
}