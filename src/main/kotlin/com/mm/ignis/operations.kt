package com.mm.ignis


fun <T1, T2, T3> pipeline(input:T1, f1: (T1) -> T2, f2: (T2) -> T3): T3 {
    return configurePipeline<T1, T3>(w(f1), w(f2))(input)
}

fun <I, O> configurePipeline(vararg actions: (Any?) -> Any?): (I) -> O = { input: I ->
    var currentInput: Any? = input

    for (action in actions) {
        val output = action(currentInput)
        currentInput = output
    }

    currentInput as O
}


fun op1(i: Int): Long { return (i + 1).toLong() }
fun op2(i: Long): Long { return i + 1 }
fun <I, O> w(f: (I) -> O): (Any?) -> Any? = { input ->
    f(input as I)
}

fun main(args: Array<String>) {
    println(pipeline(10, ::op1, ::op2))
}
