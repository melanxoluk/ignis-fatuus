package com.mm.ignis

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.SendMessage
import org.gitlab4j.api.webhook.Event
import org.gitlab4j.api.webhook.PipelineEvent
import org.gitlab4j.api.webhook.PushEvent
import org.jdbi.v3.core.mapper.Nested
import org.slf4j.LoggerFactory
import java.time.Instant


data class GitlabNotifier(
    val id: Long,
    @Nested("tb")
    val telegramBotInfo: TelegramBotInfo,
    @Nested("gh")
    val gitlabHook: GitlabProjectHook,
    val name: String,
    val channel: String,
    val createdOn: Instant = Instant.now()
)


class Notifier {
    private companion object {
        private val log = LoggerFactory.getLogger(Notifier::class.java)
        private val mapper = jacksonObjectMapper()
    }

    private val bots = mutableMapOf<String, TelegramBot>()

    fun notify(notifier: GitlabNotifier, event: Event) {
        // search project before reading content
        val telegram = bots.compute(notifier.telegramBotInfo.accessKey) {
            k, b -> b ?: TelegramBot(k)
        }!!

        val context = Context(notifier, telegram)

        when (event.objectKind) {
            PushEvent.OBJECT_KIND -> context.notifyPush(event as PushEvent)
            PipelineEvent.OBJECT_KIND -> context.notifyPipeline(event as PipelineEvent)

            // todo: determine correct flow
            else -> {
                log.info("unprocessed hook \n$event")
                context.notifyStrange(event)
            }
        }
    }


    // ~~~ executions over context

    private class Context(val notifier: GitlabNotifier, val bot: TelegramBot)

    private fun Context.notifyStrange(thing: Any) {
        sendToChannel("strange thing happened: ${mapper.writeValueAsString(thing)}")
    }

    private fun Context.notifyPush(event: PushEvent) {
        val author = event.commits[0].author.name
        val amount = event.totalCommitsCount
        val project = event.project.name
        val branch = event.branch
        val url = event.commits[0].url
        val messages = event.commits.take(10).joinToString("\n") { "| ${it.message}" }
        sendToChannel("""
            |*$author* pushed $amount commits in *$project/$branch*
            |```$messages```
            |$url
            |""".trimMargin()
        )
    }

    private fun Context.notifyPipeline(event: PipelineEvent) {
        // todo extend
        val author = event.user.name ?: "unknown"
        val status = event.objectAttributes.status ?: "unknown"
        val project = event.project.name ?: "unknown"
        val branch = event.objectAttributes.ref ?: "unknown"
        val seconds = event.objectAttributes.duration ?: 0
        sendToChannel("""
            |*${status.capitalize()}*: *$project/$branch pipeline finished in ${duration(seconds)}
            |*author:* $author
            |""".trimMargin()
        )
    }

    private fun Context.sendToChannel(text: String) {
        try {
            bot.execute(
                SendMessage(notifier.channel, text)
                    .parseMode(ParseMode.Markdown)
                    .disableWebPagePreview(true))
        } catch (ex: Exception) {
            log.error("telegram message", ex)
        }
    }


    // ~~~ misc

    private fun duration(seconds: Int): String {
        return when {
            seconds < 60 -> "${seconds}s"
            else -> "${seconds/60}m ${seconds%60}s"
        }
    }
}
