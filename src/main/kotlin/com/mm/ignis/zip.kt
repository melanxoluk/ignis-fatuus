package com.mm.ignis

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import java.util.zip.ZipFile


object WalkZipDirectory {
    fun walkFile(file: Path, handler: (ByteArray, String) -> Unit) {
        val zipis = ZipFile(file.toFile())
        for (nextEntry in zipis.entries()) {
            val bytes = zipis.getInputStream(nextEntry).readBytes()
            handler(bytes, nextEntry.name)
        }
        zipis.close()
    }

    fun walkTempFile(content: ByteArray, handler: (ByteArray, String) -> Unit) {
        val filename = "${UUID.randomUUID()}.zip"
        val path = Paths.get(filename)
        try {
            Files.write(path, content)
            walkFile(path, handler)
        } catch (ex: Throwable) {
            ex.printStackTrace()
        } finally {
            Files.delete(path)
        }
    }
}
