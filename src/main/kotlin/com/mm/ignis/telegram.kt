package com.mm.ignis

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.request.ParseMode
import com.pengrad.telegrambot.request.SendMessage
import okhttp3.OkHttpClient
import org.gitlab4j.api.webhook.PipelineEvent
import org.gitlab4j.api.webhook.PushEvent
import org.slf4j.LoggerFactory
import java.net.InetSocketAddress
import java.net.Proxy
import java.time.Instant
import java.util.concurrent.TimeUnit


data class TelegramBotInfo(
    val id: Long = -1,
    val name: String,
    val accessKey: String,
    val createdOn: Instant = Instant.now())

data class TelegramNotificationChannel(
    val id: Long = -1,
    val name: String,
    val accessKey: String,
    val channelId: String,
    val createdOn: Instant)


class Telegram(private val conf: IgnisConfiguration) {
    private companion object {
        private val log = LoggerFactory.getLogger(Telegram::class.java)
        private val mapper = jacksonObjectMapper()
    }


    private val bot = conf.telegram.run {
        TelegramBot.Builder(botAccessToken).apply {
            if (useProxy) {
                okHttpClient(
                    OkHttpClient.Builder()
                        .connectTimeout(75, TimeUnit.SECONDS)
                        .readTimeout(75, TimeUnit.SECONDS)
                        .proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress(proxyIp, proxyPort)))
                        .build()
                )
            }
        }.build()
    }

    fun notifyPush(event: PushEvent) {
        val author = event.commits[0].author.name
        val project = event.project.pathWithNamespace
        sendToChannel("*$author* pushed in *$project*")
    }

    fun notifyPipelineSuccess(event: PipelineEvent) {
        sendToChannel("build success, start deploy")
    }

    fun notifyPipelineFailed(event: PipelineEvent) {
        sendToChannel("build failed")
    }

    fun notifySiteCreated(site: IgnisProjectSite) {
        sendToChannel("site deployed")
    }

    fun notifySiteDeployed(site: IgnisProjectSite) {
        sendToChannel("site deployed")
    }

    fun notifyStrange(thing: Any) {
        sendToChannel("strange thing happened: ${mapper.writeValueAsString(thing)}")
    }

    private fun sendToChannel(text: String) {
        try {
            bot.execute(
                SendMessage(conf.telegram.channel, text)
                    .parseMode(ParseMode.Markdown)
                    .disableWebPagePreview(true))
        } catch (ex: Exception) {
            log.error("telegram message", ex)
        }
    }


    // ~~~ notification channel funcs

    fun send(channels: List<TelegramNotificationChannel>, message: String) {
        for (channel in channels) {
            send(channel, message)
        }
    }

    fun send(channel: TelegramNotificationChannel, message: String) = with(channel) {
        TelegramBot(accessKey).execute(
            SendMessage(channelId, message)
                .disableWebPagePreview(true))
    }
}
