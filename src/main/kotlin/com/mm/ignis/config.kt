package com.mm.ignis

import com.typesafe.config.ConfigFactory
import io.github.config4k.extract
import java.io.File


fun parseIgnisConfiguration(args: Array<String> = arrayOf()): IgnisConfiguration {
    var configFilename = "ignis.conf"
    if (args.isNotEmpty()) {
        configFilename = args[0]
    }

    val file = File(configFilename)
    if (!file.exists()) {
        throw RuntimeException("ignis.conf not found")
    }

    val config = ConfigFactory.parseFile(file)
    return config.extract("ignis")
}
