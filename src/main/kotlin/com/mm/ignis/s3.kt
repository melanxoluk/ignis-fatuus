package com.mm.ignis

import org.apache.tika.Tika
import org.slf4j.LoggerFactory
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.*
import java.lang.Exception


class S3(private val settings: IgnisSettings) {
    private companion object {
        private val log = LoggerFactory.getLogger(S3::class.java)
    }


    private val s3 =
        S3Client.builder()
            .credentialsProvider(
                StaticCredentialsProvider.create(
                    AwsBasicCredentials.create(
                        settings.awsAccessKeyId,
                        settings.awsSecretAccessKey)))
            .region(settings.awsRegion)
            .build()


    fun createSiteBucket(bucket: String) {
        s3.createBucket(
            CreateBucketRequest
                .builder()
                .bucket(bucket)
                .build())

        s3.putBucketWebsite(
            PutBucketWebsiteRequest
                .builder()
                .bucket(bucket)
                .websiteConfiguration(
                    WebsiteConfiguration
                        .builder()
                        .indexDocument(
                            IndexDocument
                                .builder()
                                .suffix("index.html")
                                .build())
                        .build())
                .build())

        s3.putBucketPolicy(
            PutBucketPolicyRequest
                .builder()
                .bucket(bucket)
                .policy(policy(bucket))
                .build())

        s3.putBucketAcl(
            PutBucketAclRequest
                .builder()
                .bucket(bucket)
                .acl(BucketCannedACL.PUBLIC_READ)
                .build())
    }

    fun deploySite(bytes: ByteArray, url: String) {
        val tika = Tika()

        WalkZipDirectory.walkTempFile(bytes) { content, name ->
            val name = name.replace("build/", "")
            if (name.isNotEmpty()) {
                val contentType = tika.detect(name)
                log.info("CONTENT TYPE: name=$name cp=$contentType")

                val putObject =
                    PutObjectRequest
                        .builder()
                        .bucket(url)
                        .key(name)
                        .contentType(contentType)

                s3.putObject(
                    putObject.build(),
                    RequestBody.fromBytes(content))
            }
        }
    }

    fun isBucketExists(url: String): Boolean {
        return try {
            s3.headBucket(
                HeadBucketRequest
                    .builder()
                    .bucket(url)
                    .build())
            true
        } catch (ex: Exception) {
            false
        }
    }

    fun canCreateBucket(url: String): Boolean {
        return try {
            s3.headBucket(
                HeadBucketRequest
                    .builder()
                    .bucket(url)
                    .build())
            false
        } catch (ex: NoSuchBucketException) {
            true
        }
    }

    fun deleteBucket(url: String) {
        s3.deleteBucket(DeleteBucketRequest.builder().bucket(url).build())
    }

    fun clearBucket(url: String) {
        val objects = s3.listObjectsV2(
            ListObjectsV2Request.builder().bucket(url).build())

        val ids = objects.contents().map {
            ObjectIdentifier.builder().key(it.key()).build()
        }

        if (ids.isEmpty()) return

        val delete = Delete.builder().objects(ids).build()

        s3.deleteObjects(
            DeleteObjectsRequest
                .builder()
                .bucket(url)
                .delete(delete)
                .build())
    }

    fun s3SiteName(bucketName: String) =
        "http://$bucketName.s3-website-${settings.awsRegion.id()}.amazonaws.com"

    fun s3SiteNameNoSchema(bucketName: String) =
        "$bucketName.s3-website-${settings.awsRegion.id()}.amazonaws.com"

    fun policy(bucketName: String) =
        """
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "PublicReadGetObject",
                    "Effect": "Allow",
                    "Principal": "*",
                    "Action": "s3:GetObject",
                    "Resource": "arn:aws:s3:::$bucketName/*"
                }
            ]
        }
        """.trimIndent()
}
