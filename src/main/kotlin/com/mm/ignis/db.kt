package com.mm.ignis

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import eu.roboflax.cloudflare.objects.zone.Zone
import org.gitlab4j.api.models.Project
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.KotlinPlugin
import org.jdbi.v3.core.kotlin.mapTo
import org.jdbi.v3.core.kotlin.withHandleUnchecked
import org.jdbi.v3.core.statement.SqlLogger
import org.jdbi.v3.core.statement.StatementContext
import org.jdbi.v3.postgres.PostgresPlugin
import org.slf4j.LoggerFactory
import software.amazon.awssdk.regions.Region
import java.sql.*
import java.time.Instant


class Database(private val conf: IgnisConfiguration.JdbcConf) {
    private companion object {
        private val log = LoggerFactory.getLogger(Database::class.java)
    }


    private val hikari = HikariDataSource(HikariConfig().apply {
        jdbcUrl = conf.url
        username = conf.user
        password = conf.password
    })

    private val jdbi: Jdbi =
        Jdbi.create(hikari)
            .installPlugin(PostgresPlugin())
            .installPlugin(KotlinPlugin())!!
            .registerColumnMapper(Region::class.java) { rs, index, _ ->
                Region.of(rs.getString(index))
            }
            .setSqlLogger(object : SqlLogger {
                override fun logException(context: StatementContext, ex: SQLException) {
                    log.error("rendered sql ${context.renderedSql}")
                    log.error("raw sql ${context.rawSql}")
                }
            })

    private inline fun <reified T : Any> String.mapSingle(): T =
        jdbi.withHandle<T, Exception> { h ->
            h.createQuery(this)
                .mapTo(T::class.java)
                .one()
        }

    val ignisSettings: IgnisSettings by lazy {
        "select aws_access_key_id, aws_secret_access_key, aws_region from ignis_settings".mapSingle<IgnisSettings>()
    }


    // ~~~ gitlab

    fun saveGitlabService(service: GitlabService): GitlabService {
        val createdOn = Instant.now()

        val id = "insert into gitlab_services values (default, ?, ?, ?, ?)"
            .psWithIdHikari({ ps ->
                ps.setString(1, service.name)
                ps.setString(2, service.url)
                ps.setString(3, service.accessKey)
                ps.setTimestamp(4, Timestamp.from(createdOn))
            }, { rs -> rs.getLong(1) })

        return service.copy(id = id, createdOn = createdOn)
    }

    fun selectGitlabService(id: Long) =
        "select * from gitlab_services where id = $id".querySingleHikari(::mapGitlabService)

    fun selectGitlabService(project: GitlabProject) =
        "select * from gitlab_services where id = ${project.gitlabServiceId}".querySingleHikari(::mapGitlabService)

    fun selectGitlabServices(): List<GitlabService> =
        "select * from gitlab_services".queryListHikari(::mapGitlabService)

    fun selectGitlabProject(id: Long): GitlabProject =
        "select * from gitlab_projects where id = $id".querySingleHikari(::mapGitlabProject)

    fun insertGitlabService(service: GitlabService): GitlabService {
        val id = "insert into gitlab_services values (default, ?, ?, ?, ?)"
            .psWithIdHikari({ ps ->
                ps.setString(1, service.name)
                ps.setString(2, service.url)
                ps.setString(3, service.accessKey)
                ps.setTimestamp(4, Timestamp.from(service.createdOn))
            }, { rs -> rs.getLong(1) })

        return service.copy(id = id)
    }

    fun deleteGitlabService(id: Long) {
        "delete from gitlab_services where id = $id".executeHikari()
    }

    fun insertGitlabProject(service: GitlabService, project: Project): GitlabProject {
        if (service.id < 0) throw RuntimeException("service not saved $service")

        val savedOn = Instant.now()

        val id = "insert into gitlab_projects values (default, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            .psWithIdHikari({ ps ->
                ps.setLong(1, service.id)
                ps.setLong(2, project.id.toLong())
                ps.setString(3, project.name)
                ps.setString(4, project.nameWithNamespace)
                ps.setString(5, project.path)
                ps.setString(6, project.pathWithNamespace)
                ps.setString(7, project.webUrl)
                ps.setTimestamp(8, Timestamp.from(project.createdAt.toInstant()))
                ps.setTimestamp(9, Timestamp.from(savedOn))
            }, { rs -> rs.getLong(1) })

        return GitlabProject(
            id, service.id, project.id,
            project.name, project.nameWithNamespace,
            project.path, project.pathWithNamespace,
            project.webUrl, project.createdAt.toInstant(), savedOn
        ).apply { gitlabService = service }
    }

    fun deleteGitlabProject(gitlabProject: GitlabProject) =
        "delete from gitlab_projects where id=${gitlabProject.id}".executeHikari()

    fun insertGitlabHook(hook: GitlabProjectHook): GitlabProjectHook =
        "insert into gitlab_projects_hooks values (default, :gitlabProject, :originId, :key, :url)".insert(hook)

    fun selectGitlabHook(key: String): GitlabProjectHook? =
        "select * from gitlab_projects_hooks where key = '$key'".singleOrNull()

    private fun mapGitlabProject(rs: ResultSet) =
        GitlabProject(
            rs.getLong(1),
            rs.getLong(2),
            rs.getInt(3),
            rs.getString(4),
            rs.getString(5),
            rs.getString(6),
            rs.getString(7),
            rs.getString(8),
            rs.getTimestamp(9).toInstant(),
            rs.getTimestamp(10).toInstant()
        )

    private fun mapGitlabService(rs: ResultSet) =
        GitlabService(
            rs.getLong(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
            rs.getTimestamp(5).toInstant()
        )


    // ~~~ cloudflare

    fun selectCloudflareServices() =
        "select * from cloudflare_services".queryListHikari(::mapCloudflareService)

    fun selectCloudflareService(id: Long) =
        "select * from cloudflare_services where id = $id".querySingleHikari(::mapCloudflareService)

    fun saveCloudflareSite(service: CloudflareService, zone: Zone): CloudflareSite {
        val found = findCloudflareSite(zone)
        if (found != null) return found

        if (service.id < 0) throw RuntimeException("CloudflareService id < 0")

        val savedOn = Instant.now()

        val id =
            "insert into cloudflare_sites values (default, ?, ?, ?, ?, ?)"
                .psWithIdHikari({ ps ->
                    ps.setLong(1, service.id)
                    ps.setString(2, zone.id)
                    ps.setString(3, zone.name)
                    // fixme
                    // ps.setTimestamp(4, zone.createdOn)
                    ps.setTimestamp(4, Timestamp.from(savedOn))
                    ps.setTimestamp(5, Timestamp.from(savedOn))
                }, { rs -> rs.getLong(1) })

        return CloudflareSite(id, service.id, zone.id, zone.name, savedOn, savedOn)
            .apply { cloudflareService = service }
    }

    fun selectCloudflareSite(id: Long) =
        "select * from cloudflare_sites where id = $id".querySingleHikari(::mapCloudflareSite)

    private fun findCloudflareSite(zone: Zone) =
        "select * from cloudflare_sites where origin_id = '${zone.id}'"
            .querySingleOrNullHikari(::mapCloudflareSite)

    private fun mapCloudflareService(rs: ResultSet) =
        CloudflareService(
            rs.getLong(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
            rs.getTimestamp(5).toInstant()
        )

    private fun mapCloudflareSite(rs: ResultSet) =
        CloudflareSite(
            rs.getLong(1),
            rs.getLong(2),
            rs.getString(3),
            rs.getString(4),
            rs.getTimestamp(5).toInstant(),
            rs.getTimestamp(6).toInstant()
        ).apply {
            cloudflareService = selectCloudflareService(rs.getLong(2))
        }


    // ~~~ ignis projects

    fun selectIgnisProjects(): List<IgnisProject> =
        "select * from ignis_projects".queryListHikari(::mapIgnisProject)

    fun selectIgnisProject(id: Long): IgnisProject =
        "select * from ignis_projects where id = $id".querySingleHikari(::mapIgnisProject)

    fun findIgnisProjectByKey(key: String): IgnisProject? =
        "select * from ignis_projects where gitlab_hook_key = '$key'"
            .querySingleOrNullHikari(::mapIgnisProject)

    fun findIgnisProjectSiteByBranchName(ignisProject: IgnisProject, branch: String): IgnisProjectSite? =
        "select * from ignis_projects_sites where ignis_project = ${ignisProject.id} and branch_name = '$branch'"
            .querySingleOrNullHikari(::mapIgnisProjectSite)

    fun selectProjectSites(ignisProject: IgnisProject): List<IgnisProjectSite> =
        "select * from ignis_projects_sites where ignis_project = ${ignisProject.id}".queryListHikari(::mapIgnisProjectSite)

    fun projectExists(urlName: String, siteName: String): Boolean {
        return """
            select * from ignis_projects ip
            left join cloudflare_sites cs on ip.cloudflare_site = cs.id
            where ip.url_name = '$urlName' and cs.name = '$siteName'
            """.queryHikari { rs -> return@queryHikari rs.next() }
    }

    fun saveProject(ignisProject: IgnisProject): IgnisProject {
        val id = "insert into ignis_projects values (default, ?, ?, ?, ?, ?, ?, ?, ?)"
            .psWithIdHikari({ ps ->
                ps.setLong(1, ignisProject.gitlabProject.id)
                ps.setLong(2, ignisProject.cloudflareSite.id)
                ps.setString(3, ignisProject.name)
                ps.setString(4, ignisProject.urlName)
                ps.setString(5, ignisProject.gitlabHookKey)
                ps.setLong(6, ignisProject.gitlabHookId)
                ps.setString(7, ignisProject.gitlabHookUrl)
                ps.setTimestamp(8, Timestamp.from(ignisProject.createdOn))
            }, { rs -> rs.getLong(1) })

        return ignisProject.copy(id = id)
    }

    fun updateGitlabProject(ignisProjectId: Long, gitlabProject: GitlabProject): Unit =
        "update ignis_projects set gitlab_project = ${gitlabProject.id} where id = $ignisProjectId".executeHikari()

    fun saveProjectSite(site: IgnisProjectSite): IgnisProjectSite {
        val id = "insert into ignis_projects_sites values (default, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            .psWithIdHikari({ ps ->
                ps.setLong(1, site.ignisProjectId)
                ps.setString(2, site.branchName)
                ps.setString(3, site.bucketName)
                ps.setString(4, site.dnsRecordOriginId)
                ps.setString(5, site.dnsRecordType)
                ps.setString(6, site.dnsRecordName)
                ps.setString(7, site.dnsRecordContent)
                ps.setTimestamp(8, Timestamp.from(site.savedOn))
                ps.setTimestamp(9, Timestamp.from(site.savedOn))
            }, { rs -> rs.getLong(1) })

        return site.copy(id = id)
    }

    fun deleteIgnisProject(project: IgnisProject) =
        "delete from ignis_projects where id = ${project.id}".executeHikari()

    fun deleteIgnisProjectSite(site: IgnisProjectSite) =
        "delete from ignis_projects_sites where id = ${site.id}".executeHikari()

    fun mapIgnisProject(rs: ResultSet) =
        IgnisProject(
            rs.getLong(1),
            selectGitlabProject(rs.getLong(2)),
            selectCloudflareSite(rs.getLong(3)),
            rs.getString(4),
            rs.getString(5),
            rs.getString(6),
            rs.getLong(7),
            rs.getString(8),
            rs.getTimestamp(9).toInstant()
        )

    fun mapIgnisProjectSite(rs: ResultSet) =
        IgnisProjectSite(
            rs.getLong(1),
            rs.getLong(2),
            rs.getString(3),
            rs.getString(4),
            rs.getString(5),
            rs.getString(6),
            rs.getString(7),
            rs.getString(8),
            rs.getTimestamp(9).toInstant(),
            rs.getTimestamp(10).toInstant())


    // ~~~ notifier project

    fun insertGitlabNotifier(notifier: GitlabNotifier): GitlabNotifier =
        ("insert into gitlab_notifiers values (" +
            "default, :telegramBotInfo.id, :gitlabHook.id, :name, :channel, default)"
                ).insertId(notifier).run { notifier.copy(id = this) }

    fun selectNotifiersByHookId(id: Long): List<GitlabNotifier> =
        jdbi.withHandleUnchecked { handle ->
            handle.createQuery(
                """select
                    gn.id, gn.telegram_bot, gn.gitlab_hook, gn.name, gn.channel, gn.created_on,
                    tb.id as tb_id, tb.name as tb_name, tb.access_key as tb_access_key, tb.created_on as tb_created_on,
                    gh.id as gh_id, gh.gitlab_project as gh_gitlab_project, gh.origin_id as gh_origin_id, gh.key as gh_key, gh.url as gh_url
                from
                    gitlab_notifiers gn
                        left join telegram_bots tb on gn.telegram_bot = tb.id
                        left join gitlab_projects_hooks gh on gn.gitlab_hook = gh.id
                where
                    gn.gitlab_hook = $id"""
            ).mapTo<GitlabNotifier>().list()
        }


    // ~~~ telegram bots

    fun selectTelegramBot(name: String): TelegramBotInfo? =
        "select * from telegram_bots where name = '$name'".singleOrNull()

    fun selectTelegramChannels(project: IgnisProject): List<TelegramNotificationChannel> =
        ("select id, name, access_key, channel_id, created_on " +
            "from ignis_project_telegram_notification_channels it " +
            "left join telegram_notification_channel t " +
            "on it.telegram_notification_channel = t.id " +
            "where it.ignis_project = ${project.id}").queryListHikari(::mapTelegramChannel)


    private fun mapTelegramChannel(rs: ResultSet) =
        TelegramNotificationChannel(
            rs.getLong(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
            rs.getTimestamp(5).toInstant())


    // ~~~ misc for jdbi

    private inline fun <reified E : Any> String.insert(entity: E): E =
        jdbi.withHandleUnchecked { handle ->
            handle
                .createUpdate(this)
                .bindBean(entity)
                .executeAndReturnGeneratedKeys()
                .mapTo<E>()
                .one()
        }

    private inline fun <reified E : Any> String.insertId(entity: E): Long =
        jdbi.withHandleUnchecked { handle ->
            handle
                .createUpdate(this)
                .bindBean(entity)
                .executeAndReturnGeneratedKeys()
                .map { rs, ctx -> rs.getLong(1) }
                .one()
        }

    private inline fun <reified E : Any> String.singleOrNull(): E? =
        jdbi.withHandleUnchecked { handle ->
            handle
                .createQuery(this)
                .mapTo<E>()
                .findOne()
                .orElse(null)
        }

    private inline fun <reified E : Any> String.list(): List<E> =
        jdbi.withHandleUnchecked { handle ->
            handle
                .createQuery(this)
                .mapTo<E>()
                .list()
        }


    // ~~~ misc @Deprecated use jdbi instead over

    private fun <I> String.psWithIdHikari(psf: (PreparedStatement) -> Unit, rsf: (ResultSet) -> I): I {
        hikari.connection.use { conn ->
            val ps = conn.prepareStatement(this, Statement.RETURN_GENERATED_KEYS)
            psf(ps)
            ps.execute()
            if (!ps.generatedKeys.next()) throw RuntimeException("psWithId no generated keys")
            return rsf(ps.generatedKeys)
        }
    }

    private fun <T> String.queryHikari(f: (ResultSet) -> T): T {
        hikari.connection.use { conn ->
            return f(conn.createStatement().executeQuery(this))
        }
    }

    private fun <T> String.querySingleHikari(f: (ResultSet) -> T): T {
        hikari.connection.use { conn ->
            val rs = conn.createStatement().executeQuery(this)
            if (!rs.next()) throw RuntimeException("querySingle empty sql: $this")
            val item = f(rs)
            if (rs.next()) throw RuntimeException("querySingle >1 sql: $this")
            return item
        }
    }

    private fun <T> String.querySingleOrNullHikari(f: (ResultSet) -> T): T? {
        hikari.connection.use { conn ->
            val rs = conn.createStatement().executeQuery(this)
            if (!rs.next()) return null
            val item = f(rs)
            if (rs.next()) throw RuntimeException("querySingleOrNull >1 sql: $this")
            return item
        }
    }

    private fun <T> String.queryListHikari(f: (ResultSet) -> T): List<T> {
        val items = mutableListOf<T>()
        hikari.connection.use { conn ->
            val rs = conn.createStatement().executeQuery(this)
            while (rs.next()) {
                val item = f(rs)
                items.add(item)
            }

            return items
        }
    }

    private fun String.executeHikari() {
        hikari.connection.use { conn ->
            conn.createStatement().execute(this)
        }
    }
}
