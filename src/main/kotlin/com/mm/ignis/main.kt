package com.mm.ignis

import io.ktor.server.engine.*
import io.ktor.server.netty.Netty


// make a little bit of shit right now, to prepare to bright future of project understanding
typealias HttpFactory = (ApplicationEngineEnvironmentBuilder.() -> Unit) -> ApplicationEngine

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val ignisConf = parseIgnisConfiguration(args)
        val db = Database(ignisConf.jdbc)
        val s3 = S3(db.ignisSettings)
        val httpFactory: HttpFactory = { b -> httpFactory(ignisConf.http, b)() }
        Ignis(ignisConf, db, httpFactory, Cloudflare(db), Gitlab(db), s3)

        // todo notify that application started successfully with some details about
        // todo setup shutdown hook to notify from application
    }

    fun httpFactory(
        httpConf: IgnisConfiguration.HttpConf,
        builder: ApplicationEngineEnvironmentBuilder.() -> Unit
    ): () -> ApplicationEngine {
        return {
            embeddedServer(Netty, applicationEngineEnvironment {
                connector {
                    host = httpConf.host
                    port = httpConf.port
                }

                builder()
            })
        }
    }
}
