package com.mm.ignis

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import eu.roboflax.cloudflare.objects.dns.DNSRecord
import eu.roboflax.cloudflare.objects.zone.Zone
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.BadRequestException
import io.ktor.features.ContentNegotiation
import io.ktor.features.NotFoundException
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.util.KtorExperimentalAPI
import org.gitlab4j.api.models.Project
import org.gitlab4j.api.models.ProjectHook
import org.gitlab4j.api.utils.JacksonJson
import org.gitlab4j.api.webhook.Event
import org.gitlab4j.api.webhook.PipelineEvent
import org.gitlab4j.api.webhook.PushEvent
import org.slf4j.LoggerFactory
import software.amazon.awssdk.regions.Region
import java.time.Instant


data class IgnisProject(
    val id: Long = -1,
    val gitlabProject: GitlabProject,
    val cloudflareSite: CloudflareSite,
    val name: String,
    val urlName: String,
    val gitlabHookKey: String,
    val gitlabHookId: Long,
    val gitlabHookUrl: String,
    val createdOn: Instant = Instant.now()
)

data class IgnisProjectSite(
    val id: Long = -1,
    val ignisProjectId: Long,
    val branchName: String,
    val bucketName: String,
    val dnsRecordOriginId: String,
    val dnsRecordType: String,
    val dnsRecordName: String,
    val dnsRecordContent: String,
    val dnsRecordCreatedOn: Instant,
    val savedOn: Instant
) {
    var ignisProject: IgnisProject? = null
}


/**
 * High level application functionality, which determines flow of user stories totally
 */
@KtorExperimentalAPI
class Ignis(
    private val conf: IgnisConfiguration,
    private val db: Database,
    private val httpFactory: HttpFactory,
    private val cloudflare: Cloudflare,
    private val gitlab: Gitlab,
    private val s3: S3
) {

    private companion object {
        private val log = LoggerFactory.getLogger(Ignis::class.java)
        private val gitlabEventsMapper = JacksonJson().objectMapper.registerKotlinModule()
    }

    private val notifier = Notifier()
    private val telegram = Telegram(conf)

    init {
        httpFactory {
            module {
                install(ContentNegotiation) {
                    jackson {
                        configure(SerializationFeature.FAIL_ON_SELF_REFERENCES, false)
                    }
                }

                routing {
                    get("/ping") { call.respond("pong") }

                    route("/gitlab") {
                        route("/services") {
                            get { call.respond(db.selectGitlabServices()) }

                            get("/create") {
                                val name = call.request.queryParameters["name"]!!
                                val url = call.request.queryParameters["url"]!!
                                val accessKey = call.request.queryParameters["accessKey"]!!

                                call.respond(
                                    db.insertGitlabService(
                                        GitlabService(
                                            name = name,
                                            url = url,
                                            accessKey = accessKey)))
                            }

                            get("/remove") {
                                val id = call.request.queryParameters["id"]!!
                                db.deleteGitlabService(id.toLong())
                                call.respond(HttpStatusCode.OK)
                            }
                            
                            get("/:id/projects") {}
                        }
                    }

                    route("/cloudflare") {
                        route("/services") {
                            get { call.respond(db.selectCloudflareServices()) }
                            get("/:id/projects") {}
                        }

                    }

                    // ignis projects
                    route("/projects") {
                        get {
                            call.respond(db.selectIgnisProjects())
                        }

                        get("/create") {
                            val queries = call.request.queryParameters

                            val gitlabProjectName = queries["gitlab"] ?: return@get
                            val cloudflareSiteName = queries["site"] ?: return@get
                            val name = queries["name"] ?: return@get
                            val urlName = queries["url-name"] ?: name

                            val context =
                                IgnisProjectContext(
                                    gitlabProjectName, cloudflareSiteName, name, urlName
                                )

                            try {
                                val project = context.createProject()
                                call.respond(project)
                            } catch (ex: Exception) {
                                call.respond(HttpStatusCode.BadRequest, ex.message ?: "")
                                log.error("ignis project not created context=[$context]", ex)
                            } catch (ex: RuntimeException) {
                                call.respond(HttpStatusCode.InternalServerError)
                                log.error("ignis project not created context=[$context]", ex)
                            }
                        }

                        get("/delete") {
                            val queries = call.request.queryParameters
                            val projectId = queries["id"] ?: return@get
                            val context = createContext(db.selectIgnisProject(projectId.toLong()))
                            context.delete()
                            call.respond(HttpStatusCode.OK)
                        }

                        get("/update-gitlab-project") {
                            val ignisProjectId = call.request.queryParameters["id"]!!
                            val gitlabServiceId = call.request.queryParameters["gitlab"]!!
                            val gitlabProjectName = call.request.queryParameters["gitlab-project"]!!

                            val gitlabService = db.selectGitlabService(gitlabServiceId.toLong())
                            val project = gitlab.getProject(gitlabService, gitlabProjectName)
                            val gitlabProject = db.insertGitlabProject(gitlabService, project)
                            db.updateGitlabProject(ignisProjectId.toLong(), gitlabProject)

                            call.respond(HttpStatusCode.OK)
                        }
                    }

                    route("/notifiers") {
                        get("/create") {
                            val queries = call.request.queryParameters

                            val name = queries["name"] ?: return@get
                            val telegramBotName = queries["telegram"] ?: return@get
                            val gitlabProjectName = queries["gitlab"] ?: return@get
                            val channel = queries["channel"] ?: return@get

                            val telegramBot = db.selectTelegramBot(telegramBotName) ?: return@get
                            val (gitlabService, project) = gitlab.findProject(gitlabProjectName) ?: return@get

                            val key = randomKey(12)
                            val url = "${conf.host}/notifiers/webhooks?key=$key"
                            val hook = gitlab.createWebhook(gitlabService, project, url)

                            val gitlabProject = db.insertGitlabProject(gitlabService, project)
                            val gitlabProjectHook =
                                db.insertGitlabHook(GitlabProjectHook(-1, gitlabProject.id, hook.id, key, url))
                            val gitlabNotifier = db.insertGitlabNotifier(
                                GitlabNotifier(
                                    -1,
                                    telegramBot,
                                    gitlabProjectHook,
                                    name,
                                    channel
                                )
                            )

                            call.respond(gitlabNotifier)
                        }

                        post("/webhooks") {
                            val key = call.request.queryParameters["key"] ?: return@post
                            val gitlabHook = db.selectGitlabHook(key)
                            if (gitlabHook == null) {
                                log.error("not found gitlab hook for key $key")
                                return@post
                            }

                            val gitlabNotifiers = db.selectNotifiersByHookId(gitlabHook.id)
                            if (gitlabNotifiers.isEmpty()) {
                                log.error("no registered notifiers for hook $gitlabHook")
                                return@post
                            }

                            val event = gitlabEventsMapper.readValue(call.receiveText(), Event::class.java)
                            log.info("notifier webhook event $event")

                            try {
                                for (gitlabNotifier in gitlabNotifiers) {
                                    notifier.notify(gitlabNotifier, event)
                                }
                            } catch (e: Exception) {
                                log.error("notifier webhook event $event", e)
                                call.respond(HttpStatusCode.InternalServerError)
                            }

                            call.respond(HttpStatusCode.OK)
                        }
                    }

                    post("/webhooks") {
                        // search project before reading content
                        val key = call.request.queryParameters["key"] ?: return@post
                        val ignisProject = db.findIgnisProjectByKey(key) ?: return@post
                        val telegramChannels = db.selectTelegramChannels(ignisProject)

                        val event = gitlabEventsMapper.readValue(call.receiveText(), Event::class.java)
                        when (event.objectKind) {
                            PushEvent.OBJECT_KIND -> {
                                val pushEvent = event as PushEvent
                                // fixme push notifications are important much
                                // telegram.notifyPush(pushEvent)

                                // check existing of ignis site for branch in da push
                                val site = db.findIgnisProjectSiteByBranchName(ignisProject, pushEvent.branch)
                                if (site == null) {
                                    log.info("new branch detected, start to create site")

                                    val context = createContext(ignisProject)
                                    val newSite = context.createSite(pushEvent.branch)
                                    telegram.send(telegramChannels, "site created: ${newSite.bucketName}")

                                    // there is no need to try to deploy site of first push commit to branch
                                    // soon success pipeline is would be finished and will start deploy of site
                                }
                            }

                            PipelineEvent.OBJECT_KIND -> {
                                val pipelineEvent = event as PipelineEvent

                                val ignisProjectSite =
                                    db.findIgnisProjectSiteByBranchName(
                                        ignisProject, pipelineEvent.objectAttributes.ref)

                                if (ignisProjectSite == null) {
                                    log.error("not found ignis project site for web hook pipeline event $event")
                                    return@post
                                }

                                when (pipelineEvent.objectAttributes.status) {
                                    "success" -> {

                                        // telegram.notifyPipelineSuccess(pipelineEvent)
                                        val context = createContext(ignisProject)
                                        context.deploySite(ignisProjectSite, pipelineEvent.objectAttributes.ref)
                                        telegram.send(telegramChannels, "some site deployed")
                                        // telegram.notifySiteDeployed(ignisProjectSite)
                                    }

                                    "failed" -> {
                                        telegram.send(telegramChannels, "pipeline failed")
                                    }

                                    else -> {
                                        log.info("intermediate pipeline status ${pipelineEvent.objectAttributes.status}")
                                    }
                                }
                            }

                            // todo: determine correct flow
                            else -> {
                                log.info("unprocessed hook \n$event")
                                telegram.send(telegramChannels, "strange thing happened: ${gitlabEventsMapper.writeValueAsString(event)}")
                            }
                        }

                        call.respond(HttpStatusCode.OK)
                    }
                }
            }
        }.start()

        log.info("ignis started")
    }


    private data class IgnisProjectContext(
        val gitlabProjectName: String,
        val cloudflareSiteName: String,
        val name: String,
        val urlName: String
    ) {
        var gitlabService: GitlabService? = null
        var gitlabOriginProject: Project? = null
        var zone: Zone? = null
        var hookKey: String? = null
        var webHookUrl: String? = null
        var webHook: ProjectHook? = null
        var ignisProject: IgnisProject? = null
        var gitlabProject: GitlabProject? = null
        var cloudflareService: CloudflareService? = null
        var cloudflareSite: CloudflareSite? = null
        var dnsRecord: DNSRecord? = null
        override fun toString(): String {
            return """
                CreateIgnisProjectContext(gitlabProjectName='$gitlabProjectName', 
                cloudflareSiteName='$cloudflareSiteName', 
                name='$name', 
                urlName='$urlName', 
                gitlabOriginProject=$gitlabOriginProject, 
                zone=$zone, 
                hookKey='$hookKey', 
                webHookUrl='$webHookUrl', 
                webHook=$webHook, 
                ignisProject=$ignisProject, 
                gitlabProject=$gitlabProject, 
                cloudflareSite=$cloudflareSite, 
                dnsRecord=$dnsRecord)
                """.trim()
        }
    }

    private fun createContext(project: IgnisProject) =
        IgnisProjectContext(
            project.gitlabProject.name,
            project.cloudflareSite.name,
            project.name,
            project.urlName
        ).apply {
            ignisProject = project
            gitlabService = db.selectGitlabService(project.gitlabProject)
            gitlabOriginProject = gitlab.getProject(project.gitlabProject)

            val cf = cloudflare.findSite(project.cloudflareSite.name)!!
            cloudflareService = cf.first
            zone = cf.second
        }

    private fun IgnisProjectContext.createProject(): IgnisProject {
        // - find first gitlab project throw registered gitlab services
        val (service, project) = gitlab.findProject(gitlabProjectName)
            ?: throw NotFoundException("not found gitlab project with name $gitlabProjectName")
        gitlabService = service
        gitlabOriginProject = project

        // - find first cloudflare site throw registered cloudflare services
        val (cfService, cfZone) = cloudflare.findSite(cloudflareSiteName)
            ?: throw NotFoundException("not found cloudflare site with name $cloudflareSiteName")
        cloudflareService = cfService
        zone = cfZone

        // - check that there are no projects with similar urlName & cloudflare site
        if (db.projectExists(urlName, cloudflareSiteName)) {
            throw BadRequestException("project with url $urlName already exists for site $cloudflareSiteName")
        }

        // create gitlab hook
        hookKey = randomKey(15)
        webHookUrl = "${conf.host}/webhooks?key=$hookKey"
        webHook = gitlab.createWebhook(gitlabService!!, gitlabOriginProject!!, webHookUrl!!)

        // - save project
        ignisProject = saveIgnisProject(hookKey!!, webHook!!)

        // - get project branches from gitlab, create site for every
        val branches = gitlab.getBranches(gitlabService!!, gitlabOriginProject!!)
        for (branch in branches) {
            val site = createSite(branch.name)
            telegram.notifySiteCreated(site)

            deploySite(site, branch.name)
            telegram.notifySiteDeployed(site)
        }

        return ignisProject!!
    }

    private fun IgnisProjectContext.saveIgnisProject(key: String, webHook: ProjectHook): IgnisProject {
        gitlabProject = db.insertGitlabProject(gitlabService!!, gitlabOriginProject!!)
        cloudflareSite = db.saveCloudflareSite(cloudflareService!!, zone!!)

        val project =
            IgnisProject(
                -1, gitlabProject!!, cloudflareSite!!,
                name, urlName, key, webHook.id.toLong(), webHook.url)

        return db.saveProject(project)
    }

    private fun IgnisProjectContext.createSite(branch: String): IgnisProjectSite {
        // - form desired bucket name
        // fixme add cebab-style formatter
        val sitePrefix =
            if (branch == "master") urlName
            else "$urlName-$branch"

        val siteName = "$sitePrefix.${zone!!.name}"

        // - check that bucket exists
        if (!s3.canCreateBucket(siteName)) {
            throw BadRequestException("can't create s3 bucket $siteName")
        }

        // - if not exists create it
        s3.createSiteBucket(siteName)

        dnsRecord =
            cloudflare.createRecord(
                cloudflareService!!, zone!!,
                siteName, s3.s3SiteNameNoSchema(siteName))

        val site =
            IgnisProjectSite(
                -1, ignisProject!!.id,
                branch, siteName,
                dnsRecord!!.id,
                dnsRecord!!.type,
                dnsRecord!!.name,
                dnsRecord!!.content,
                Instant.now(),
                Instant.now())

        return db.saveProjectSite(site)
    }

    private fun IgnisProjectContext.deploySite(site: IgnisProjectSite, branch: String) {
        val artifact = gitlab.lastArtifact(gitlabService!!, gitlabOriginProject!!, branch)
        if (artifact == null) {
            log.error("no artifact for branch=$branch on context $this")
            return
        }

        s3.clearBucket(site.bucketName)
        s3.deploySite(artifact, site.bucketName)
    }

    private fun IgnisProjectContext.delete() {
        // stop to process hooks
        gitlab.removeWebHook(gitlabService!!, ignisProject!!)

        // clear sites
        val sites = db.selectProjectSites(ignisProject!!)
        for (site in sites) {
            delete(site)
        }

        // everything clear
        db.deleteIgnisProject(ignisProject!!)
        db.deleteGitlabProject(ignisProject!!.gitlabProject)
    }

    private fun IgnisProjectContext.delete(site: IgnisProjectSite) {
        cloudflare.deleteRecord(ignisProject!!, site)
        s3.clearBucket(site.bucketName)
        s3.deleteBucket(site.bucketName)
        db.deleteIgnisProjectSite(site)
    }
}

// static configuration provided on start
data class IgnisConfiguration(
    val host: String,
    val http: HttpConf,
    val jdbc: JdbcConf,
    val telegram: TelegramConf
) {
    data class HttpConf(
        val host: String,
        val port: Int
    )

    data class JdbcConf(
        val url: String,
        val driver: String,
        val user: String,
        val password: String
    )

    data class TelegramConf(
        val botAccessToken: String,
        val channel: String,
        val useProxy: Boolean,
        val proxyIp: String,
        val proxyPort: Int
    )
}

// dynamic configurable settings in runtime
data class IgnisSettings(
    val awsAccessKeyId: String,
    val awsSecretAccessKey: String,
    val awsRegion: Region
)
