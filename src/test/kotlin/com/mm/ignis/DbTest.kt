package com.mm.ignis

import io.kotlintest.specs.StringSpec


class DbTest : StringSpec({
    val config = parseIgnisConfiguration()
    val db = Database(config.jdbc)

    "error should be raised for non unique gitlab hook key" {
        db.selectNotifiersByHookId(2)
    }

    "notifier should inserts" {
        db.insertGitlabNotifier(
            GitlabNotifier(
                -1,
                db.selectTelegramBot("leandi")!!,
                db.selectGitlabHook("abc")!!,
                "temp",
                "temp"
            )
        )
    }
})
