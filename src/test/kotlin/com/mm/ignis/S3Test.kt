package com.mm.ignis

import io.kotlintest.specs.StringSpec


class S3Test:StringSpec({
    val config = parseIgnisConfiguration()
    val db = Database(config.jdbc)
    val s3 = S3(db.ignisSettings)

    "create s3 bucket with name 'example.com'" {
        s3.isBucketExists("modeler.leandi.net")
    }
})
