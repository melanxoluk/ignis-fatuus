call gradlew shadowJar -x test
bash -c "scp "$(pwd)/build/libs/ignis.jar" root@ssh.leandi.net:/root/ignis/ignis.jar"
bash -c "ssh root@ssh.leandi.net sh /root/ignis/restart.sh"
