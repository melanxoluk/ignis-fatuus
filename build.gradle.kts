import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.21"
    id("io.spring.dependency-management") version "1.0.6.RELEASE"
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

group = "com.mm"
version = "0.1"

val ktor_version = "1.2.3"

repositories {
    maven { setUrl("https://dl.bintray.com/kotlin/kotlin-eap") }
    maven { setUrl("https://jitpack.io") }
    mavenCentral()
    jcenter()
}

dependencyManagement {
    imports {
        mavenBom("software.amazon.awssdk:bom:2.5.29")
        mavenBom("org.jdbi:jdbi3-bom:3.9.0")
    }
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.3.41")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.3.41")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.3.41")

    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("io.github.config4k:config4k:0.4.1")
    // to parse mime types of files by their extension
    implementation("org.apache.tika:tika-core:1.22")

    implementation("org.postgresql:postgresql:42.2.6")
    implementation("com.zaxxer:HikariCP:3.3.1")
    implementation("org.jdbi:jdbi3-core")
    implementation("org.jdbi:jdbi3-kotlin")
    implementation("org.jdbi:jdbi3-postgres")
    implementation("com.mm:daccess:0.1.2")

    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("io.ktor:ktor-jackson:$ktor_version")
    // need force verse because aws is down without it
    implementation("org.apache.httpcomponents:httpclient:4.5.9")

    implementation("com.github.pengrad:java-telegram-bot-api:4.4.0")
    implementation("org.gitlab4j:gitlab4j-api:4.14.5")
    implementation("com.github.robinbraemer:cloudflareapi:1.3.2")
    implementation("software.amazon.awssdk:s3")

    implementation("com.github.kittinunf.fuel:fuel:2.2.0")
    implementation("com.github.kittinunf.fuel:fuel-jackson:2.2.0")

    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.0")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.apiVersion = "1.3"
    kotlinOptions.languageVersion = "1.3"
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<ShadowJar> {
    manifest {
        attributes["Main-Class"] = "com.mm.ignis.Main"
    }

    archiveBaseName.set("ignis")
    archiveClassifier.set("")
    archiveVersion.set("")
}
